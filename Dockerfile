FROM python:3.9-alpine

WORKDIR /code
COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY src src

CMD ["python", "src/main.py"]
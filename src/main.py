from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import json
import os
import requests
import smtplib

from dotenv import load_dotenv

load_dotenv()

CACHE_FILE = os.environ.get('CACHE_FILE')


def run():
    if not os.path.exists(CACHE_FILE):
        with open(CACHE_FILE, 'w') as f:
            json.dump({}, f)

    with open(CACHE_FILE, 'r') as f:
        cache = json.load(f)

    s = requests.Session()

    # Obtain cookies
    s.get('https://www.roomspot.nl/?type=20140218&content_uid=976')

    # Get login configuration
    r = s.get('https://www.roomspot.nl/portal/account/frontend/getloginconfiguration/format/json')
    if r.status_code != 200:
        send_api_error(r.status_code, r.text)
        return

    login_id = r.json()['loginForm']['id']
    login_hash = r.json()['loginForm']['elements']['__hash__']['initialData']

    r = s.post('https://www.roomspot.nl/portal/account/frontend/loginbyservice/format/json', {
        '__id__': login_id,
        '__hash__': login_hash,
        'username': os.environ.get('ACCOUNT_USERNAME'),
        'password': os.environ.get('ACCOUNT_PASSWORD')
    }, headers={
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    })

    if r.status_code != 200:
        send_api_error(r.status_code, r.text)
        return

    r = s.get('https://www.roomspot.nl/portal/registration/frontend/getactievereacties/format/json', data={
        'page': '1',
        'limit': '0',
        'sort': 'reactiedatum-'
    }, headers={
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    })

    if r.status_code != 200:
        send_api_error(r.status_code, r.text)
        return

    responses = r.json()['result']['items']
    changed_responses = []
    new_cache = {}
    for response in responses:
        id = str(response['id'])
        position = response['positie']
        if id in cache.keys() and cache[id] != position:
            changed_responses.append(response)
        new_cache[id] = position

    if len(changed_responses) > 0:
        print('Sending changed position email...')
        send_email('Je positie is veranderd', """Hoi Nikster,

Helaas, je positie is veranderd:

{}

""".format('\n\n'.join([f"Woning: {response['object']['street']} {response['object']['houseNumber']}-{response['object']['houseNumberAddition']}\nPositie: {response['positie']}\nURL: https://www.roomspot.nl/aanbod/te-huur/details/{response['object']['urlKey']}" for response in changed_responses])))

    with open(CACHE_FILE, 'w') as f:
        json.dump(new_cache, f)


def send_api_error(status_code, body):
    print('Sending error email...')
    send_email('An error occurred', f"""Hi Nikster,

    Positionbot encountered the following problem trying to login:

    Status code: {status_code}
    Body: {body}
    """)


def send_email(subject, body):
    message = MIMEMultipart()
    message['From'] = os.environ.get('MAIL_FROM')
    message['To'] = os.environ.get('MAIL_TO')
    message['Subject'] = subject
    message.attach(MIMEText(body, 'plain'))
    text = message.as_string()

    session = smtplib.SMTP(os.environ.get('MAIL_HOST'), int(os.environ.get('MAIL_PORT')))
    session.starttls()
    session.login(os.environ.get('MAIL_USERNAME'), os.environ.get('MAIL_PASSWORD'))
    session.sendmail(os.environ.get('MAIL_FROM'), os.environ.get('MAIL_TO'), text)
    session.quit()


if __name__ == '__main__':
    run()
